#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gmap.h"

// binary pgm header
void p5_header(FILE *f,int w,int h,int max)
{
    fprintf(f,"P5\n%d %d\n%d\n",w,h,max);
}

// binary ppm header
void p6_header(FILE *f,int w,int h,int max)
{
    fprintf(f,"P6\n%d %d\n%d\n",w,h,max);
}

void gmap_destroy(gmap_t *g)
{
    free(g);
}

gmap_t *gmap_create(int w,int h)
{
    size_t sz = sizeof(gmap_t)+w*h*sizeof(double);
    gmap_t *g = malloc(sz);
    if(g) {
        g->w = w;
        g->h = h;
        g->sz = sz;
    }
    return g;
}

gmap_t *gmap_copy(gmap_t *g)
{
    gmap_t *d = malloc(g->sz);
    if(d) {
        memcpy(d, g, g->sz);
    }
    return d;
}


void gmap_clear(gmap_t *g)
{
    int x,y;
    for(y=0; y<g->h; y++) {
        double *p = &g->p[y*g->w];
        for(x=0;x<g->w;x++) {
            p[x] = 0.0f;
        }
    }
}

int max3(int a, int b, int c)
{
    int m = a > b ? a : b;
    m = m > c ? m : c;
    return m;
}

int clampubyte(double x)
{
    return x<0 ? 0 : x>255 ? 255 : x;
}

void gmap_save_rgb(gmap_t *r,gmap_t *g, gmap_t *b, const char *fname)
{
    int x,y;
    FILE *f = fopen(fname,"w");
    int w = max3(r->w, g->w, b->w);
    int h = max3(r->h, g->h, b->h);

    p6_header(f,w,h,255);
    for(y = 0;y < h;y++) {
        for(x = 0;x < w;x++) {
            double cr = x < r->w ? r->p[y*r->w+x]*255 : 0;
            double cg = x < g->w ? g->p[y*g->w+x]*255 : 0;
            double cb = x < b->w ? b->p[y*b->w+x]*255 : 0;
            fputc(clampubyte(cr),f);
            fputc(clampubyte(cg),f);
            fputc(clampubyte(cb),f);
        }
    }
    fclose(f);
}


void gmap_save(gmap_t *g, const char *fname)
{
    int i;
    FILE *f = fopen(fname,"w");
    p5_header(f,g->w,g->h,255);
    for(i=0; i<g->w*g->h; i++) {
        double x = g->p[i]*255;
        int c = x<0 ? 0 : x>255 ? 255 : x;
        fputc(c,f);
    }
    fclose(f);
}

void gmap_save_csv(gmap_t *g, const char *fname)
{
    int x,y,i;
    FILE *f = fopen(fname,"w");
    i = 0;
    for(y = 0;y < g->h;y++) {
        for(x = 0;x < g->w;x++) {
            fprintf(f, "%f%s", g->p[i], x == g->w - 1 ? "\n" : ",");
            ++i;
        }
    }
    fclose(f);
}

void gmap_set(gmap_t *g,int x,int y,double v)
{
    g->p[x + y*g->w] = v;
}

void gmap_iloopf2(gmap_t *g, double (*func2)(double,double),double v)
{
    int i;
    for(i=0; i<g->w*g->h; i++) {
        g->p[i] = func2(g->p[i], v);
    }
}

double _add(double a, double b) { return a + b;}
double _mul(double a, double b) { return a * b;}
double _thresh(double a, double t) { return a < t ? 0.0 : 1.0; }

void gmap_iadd(gmap_t *g, double x)
{
    gmap_iloopf2(g, _add, x);
}

void gmap_imul(gmap_t *g, double x)
{
    gmap_iloopf2(g, _mul, x);
}

void gmap_ithreshold(gmap_t *g, double thresh)
{
    gmap_iloopf2(g, _thresh, thresh);
}

double gmap_min(gmap_t *g)
{
    int i;
    double r = g->p[0];
    for(i=0; i<g->w*g->h; i++) {
        double x = g->p[i];
        r = x < r ? x : r;
    }
    return r;
}

double gmap_max(gmap_t *g)
{
    int i;
    double r = g->p[0];
    for(i=0; i<g->w*g->h; i++) {
        double x = g->p[i];
        r = x > r ? x : r;
    }
    return r;
}

void gmap_iaddm(gmap_t *g,int dx,int dy,gmap_t *s)
{
    int x,y;

    for(y=0;y < s->h && y+dy < g->h;y++) {
        double *sp = &s->p[y * s->w];
        double *dp = &g->p[(y+dy) * g->w + dx];
        for(x=0;x < s->w && x+dx < g->w;x++) {
            *dp++ += *sp++;
        }
    }
}

void gmap_imulm(gmap_t *g,int dx,int dy,gmap_t *s)
{
    int x,y;

    for(y=0;y < s->h && y+dy < g->h;y++) {
        double *sp = &s->p[y * s->w];
        double *dp = &g->p[(y+dy) * g->w + dx];
        for(x=0;x < s->w && x+dx < g->w;x++) {
            *dp++ *= *sp++;
        }
    }
}


void gmap_iinvert(gmap_t *g)
{
    gmap_imul(g,-1.0);
    gmap_iadd(g,1.0);
}


void gmap_imaximize(gmap_t *g)
{
    double min = gmap_min(g);
    double max = gmap_max(g);
    double div = max-min;
    gmap_iadd(g, -min);
    if(div != 0)
        gmap_imul(g, 1.0/div);
}

// horizontal blur 'to the right'
void gmap_ihblur(gmap_t *g)
{
    int x,y;
    int i;

    i = 0;
    for(y = 0;y < g->h; y++) {
        double q = g->p[i + g->w -1];
        double rhs = g->p[i];
        for(x = 0;x < g->w - 1; x++) {
            double r = g->p[i];
            g->p[i] = (q + g->p[i+1] + 2*r)*0.25;
            q = r;
            ++i;
        }
        g->p[i] = (q + rhs + 2*g->p[i])*0.25;
        ++i;
    }
}

// vertical blur
void gmap_ivblur(gmap_t *g)
{
    int x,y;
    int i;

    for(x = 0;x < g->w; x++) {
        double q = g->p[x + g->w*(g->h-1)];
        double bot = g->p[x];
        i = x;
        for(y =0;y < g->h - 1; y++) {
            double r = g->p[i];
            g->p[i] = (q + g->p[i+g->w] + 2*r)*0.25;
            q = r;
            i += g->w;
        }
        g->p[i] = (q + bot + 2*g->p[i])*0.25;
    }
}


void gmap_iblur(gmap_t *g, int n)
{
    while(n-- > 0) {
        gmap_ihblur(g);
        gmap_ivblur(g);
    }
}

gmap_t *gmap_hhalf(gmap_t *g)
{
    gmap_t *d = gmap_create(g->w/2,g->h);
    int x,y;

    for(y=0;y < g->h;y++) {
        double *dp = &d->p[y * d->w];
        double *sp = &g->p[y * g->w];
        for(x=0;x < d->w;x++) {
            double v = 0.5*(sp[0]+sp[1]);
            *dp++ = v;
            sp += 2;
        }
    }
    return d;
}

gmap_t *gmap_vhalf(gmap_t *g)
{
    gmap_t *d = gmap_create(g->w,g->h/2);
    int x,y;

    for(y=0;y < d->h;y++) {
        double *dp = &d->p[y * d->w];
        double *sp = &g->p[y * 2 * g->w];
        for(x=0;x < d->w;x++) {
            double v = 0.5*(sp[0] + sp[g->w]);
            *dp = v;
            dp++;
            sp++;
        }
    }
    return d;
}

gmap_t *gmap_quarter(gmap_t *g)
{
    gmap_t *a = gmap_vhalf(g);
    gmap_t *b = gmap_hhalf(a);

    gmap_destroy(a);
    return b;
}

void gmap_dump(gmap_t *g)
{
    int x,y;

    printf("gmap(%p): w = %d h = %d\n",g,g->w,g->h);
    for(y=0;y<g->h;y++) {
        for(x=0;x<g->w;x++) {
            printf("%f%s",g->p[g->w*y+x],x==g->w-1?"\n" : ",");
        }
    }
}

void gmap_iloopf1(gmap_t *g,double (*func1)(double))
{
    int i;

    for(i=0; i<g->w*g->h; i++) {
        g->p[i] = func1(g->p[i]);
    }
}


double _clamp(double x)
{
    if(x < 0.0) x = 0.0;
    else if(x > 1.0) x = 1.0;
    return x;
}

double _smoothstep(double x)
{
    x = _clamp(x);
    return x * x * (3 - 2 * x);
}

void gmap_iclamp(gmap_t *g)
{
    gmap_iloopf1(g, _clamp);
}


void gmap_ismoothstep(gmap_t *g)
{
    gmap_iloopf1(g, _smoothstep);
}

void gmap_isin(gmap_t *g)
{
    gmap_iloopf1(g, sin);
}

void gmap_icos(gmap_t *g)
{
    gmap_iloopf1(g, cos);
}

void gmap_iabs(gmap_t *g)
{
    gmap_iloopf1(g, fabs);
}

void gmap_imulunoise(gmap_t *g)
{
    int i;

    for(i=0; i<g->w*g->h; i++) {
        double u = (double)rand() / RAND_MAX;
        g->p[i] = u * g->p[i];;
    }
}

double drand()
{
  return (rand() + 1.0)/(RAND_MAX + 1.0);
}

double random_normal()
{
  return sqrt(-2*log(drand())) * cos(2*M_PI*drand());
}

double gnoise(void)
{
    double n;

    do {
        n = random_normal()*0.25 + 0.5;
    } while(n < 0 || n > 1.0);
    return n;
}

void gmap_imulgnoise(gmap_t *g)
{
    int i;

    for(i=0; i<g->w*g->h; i++) {
        double n = gnoise();;
        g->p[i] = n * g->p[i];;
    }
}

void gmap_isetunoise(gmap_t *g)
{
    int i;

    for(i=0; i<g->w*g->h; i++) {
        double u = (double)rand() / RAND_MAX;
        g->p[i] = u;
    }
}


void gmap_isetgnoise(gmap_t *g)
{
    int i;

    for(i=0; i<g->w*g->h; i++) {
        double n = gnoise();
        g->p[i] = n;
    }
}
