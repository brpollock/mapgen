/*
    Tests for blur functions
    - easier to do in C as requires gmap_set()

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "gmap.h"


int main(void)
{
    int i,x,y;

    gmap_t *a = gmap_create(16,16);
    gmap_t *b = gmap_create(16,16);
    gmap_clear(a);
    gmap_clear(b);

    for(i = 0;i < 16;i++) {
        gmap_set(a,8,i,1.0);
        gmap_set(a,i<8?0:15,i,1.0);
        gmap_set(b,i,8,1.0);
        gmap_set(b,i,i<8?0:15,1.0);
    }

    gmap_save(a,"tblurh0.pgm");
    gmap_ihblur(a);
    gmap_save(a,"tblurh.pgm");

    gmap_ivblur(b);
    gmap_save(b,"tblurv.pgm");
    
    gmap_destroy(a);
    gmap_destroy(b);

    return 0;
}

