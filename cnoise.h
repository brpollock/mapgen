#ifndef _CNOISE_H_
#define _CNOISE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "gmap.h"

// worley noise

typedef struct {
    int iw;
    int ih;
    int w;
    int h;
    int div;
    gmap_t *gx;
    gmap_t *gy;
} cnoise_t;

typedef struct {
    double f[3];
} cnoise_f_t;

void cnoise_destroy(cnoise_t *c);
cnoise_t *cnoise_create(int iw, int ih, int div, int seed);
void cnoise_dump(cnoise_t *c);
cnoise_f_t cnoise_get(cnoise_t *c, int ix, int iy);
gmap_t *cnoise_render(cnoise_t *c,double c0,double c1,double c2,double k);

#endif // _CNOISE_H_
