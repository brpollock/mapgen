#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "gmap.h"
#include "cnoise.h"

// worley noise

void cnoise_destroy(cnoise_t *c)
{
    if(c) {
        gmap_destroy(c->gx);
        gmap_destroy(c->gy);
        free(c);
    }
}

cnoise_t *cnoise_create(int iw, int ih, int div, int seed)
{
    cnoise_t *c = malloc(sizeof(cnoise_t));
    if(c) {
        int p,q,i;
        c->iw = iw;
        c->ih = ih;
        c->w = iw/div+3;
        c->h = ih/div+3;
        c->div = div;
        c->gx = gmap_create(c->w,c->h);
        c->gy = gmap_create(c->w,c->h);
        if(!c->gx || !c->gy) {
            cnoise_destroy(c);
            return NULL;
        }
        srand(seed);
        i = 0;
        for(q=0; q<c->h;q++) {
            for(p=0;p<c->w;p++) {
                double cx = rand()%99 / 100.0;
                double cy = rand()%99 / 100.0;
                c->gx->p[i] = p + cx;
                c->gy->p[i] = q + cy;
                ++i;
            }
        }
    }
    return c;
}

void cnoise_dump(cnoise_t *c)
{
    printf("x:\n");
    gmap_dump(c->gx);
    printf("x:\n");
    gmap_dump(c->gy);
}

cnoise_f_t cnoise_get(cnoise_t *c, int ix, int iy)
{
    cnoise_f_t r = {{123,1234,12345}};
    int p = ix / c->div;
    int q = iy / c->div;
    double x = 1 + 1.0 * ix / c->div;
    double y = 1 + 1.0 * iy / c->div;
    int j,k;
    int b = q * c->gx->w + p;

    for(k = 0;k < 3;k++) {
        for(j = 0;j < 3;j++) {
            double dx = x - c->gx->p[b+j];
            double dy = y - c->gy->p[b+j];
            double d = sqrt((double)dx*dx + dy*dy);
//            printf("dx: %f dy: %f d: %f\n",dx,dy,d);
            if(d < r.f[0]) {r.f[2] = r.f[1]; r.f[1] = r.f[0]; r.f[0] = d;}
            else if (d < r.f[1]) {r.f[2] = r.f[1]; r.f[1] = d;}
            else if (d < r.f[2]) {r.f[2] = d;}
        }
        b += c->gx->w;
    }

//    printf("f0: %f f1: %f f2: %f\n",r.f[0],r.f[1],r.f[2]);
    return r;
}


gmap_t *cnoise_render(cnoise_t *c,double c0,double c1,double c2,double k)
{
    gmap_t *g = gmap_create(c->iw, c->ih);
    int x,y;

    for(y=0;y<g->h;y++) {
        for(x=0;x<g->w;x++) {
            double v;
            cnoise_f_t n = cnoise_get(c, x, y);
            v = k + c0*n.f[0] + c1*n.f[1] + c2*n.f[2];
            gmap_set(g, x, y, v);
        }
    }
    return g;
}

//



