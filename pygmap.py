from ctypes import *
import numbers

G = CDLL("./gmlib.so")

G.gmap_create.restype = c_void_p
G.gmap_copy.restype = c_void_p
G.gmap_hhalf.restype = c_void_p
G.gmap_vhalf.restype = c_void_p
G.gmap_quarter.restype = c_void_p

G.cnoise_create.restype = c_void_p
G.cnoise_render.restype = c_void_p


def save_rgb(r,g,b,filename):
    G.gmap_save_rgb(r.g, g.g, b.g, c_char_p(filename.encode('utf-8')))


class GMap():

    def __init__(self,width,height,g=None):
        if width and height:
            self.g = c_void_p(G.gmap_create(width,height))
            G.gmap_clear(self.g)
        else:
            self.g = g

    def __del__(self):
        G.gmap_destroy(self.g)

    def copy(self):
        a = GMap(0,0,c_void_p(G.gmap_copy(self.g)))
        return a

    def save(self, filename):
        G.gmap_save(self.g, c_char_p(filename.encode('utf-8')))

    def save_csv(self, filename):
        G.gmap_save_csv(self.g, c_char_p(filename))

    def dump(self):
        G.gmap_dump(self.g)

    def set(self,x,y,v):
        G.gmap_set(self.g,x,y,c_double(v))
        return self

    def __iadd__(self,other):
        if isinstance(other, numbers.Number):
            G.gmap_iadd(self.g, c_double(other))
        else:
            G.gmap_iaddm(self.g, 0, 0, other.g)
        return self

    def __imul__(self,other):
        if isinstance(other, numbers.Number):
            G.gmap_imul(self.g, c_double(other))
        else:
            G.gmap_imulm(self.g, 0, 0, other.g)
        return self

    def __add__(self,other):
        a = self.copy()
        a.__iadd__(other)
        return a

    def __mul__(self,other):
        a = self.copy()
        a *= other
        return a

    def __neg__(self):
        return self.copy() * -1

    def __isub__(self,other):
        return self._iadd__(-other)

    def __sub__(self,other):
        return self.__add__(-other)

    def hhalf(self):
        a = GMap(0,0,c_void_p(G.gmap_hhalf(self.g)))
        return a

    def vhalf(self):
        a = GMap(0,0,c_void_p(G.gmap_vhalf(self.g)))
        return a

    def _quarter(self):
        a = GMap(0,0,c_void_p(G.gmap_quarter(self.g)))
        return a

    def quarter(self,n):
        a = self
        while n > 0:
            a = a._quarter()
            n -= 1
        return a

    def thresholded(self,th):
        G.gmap_ithreshold(self.g,c_double(th))
        return self

    def threshold(self,th):
        a = self.copy()
        return a.thresholded(th)

    def blurred(self,n):
        G.gmap_iblur(self.g,c_int(n))
        return self

    def blur(self,n):
        a = self.copy()
        return a.blurred(n)

    def hblur(self):
        a = self.copy()
        G.gmap_ihblur(a.g)
        return a

    def vblur(self):
        a = self.copy()
        G.gmap_ivblur(a.g)
        return a

    def maximized(self):
        G.gmap_imaximize(self.g)
        return self

    def maximize(self):
        a = self.copy()
        return a.maximized()

    def inverted(self):
        G.gmap_iinvert(self.g)
        return self

    def __invert__(self):
        a = self.copy()
        return a.inverted()

    def clamp(self):
        a = self.copy()
        G.gmap_iclamp(a.g)
        return a


    def smoothstep(self):
        a = self.copy()
        G.gmap_ismoothstep(a.g)
        return a

    def sin(self):
        a = self.copy()
        G.gmap_isin(a.g)
        return a

    def cos(self):
        a = self.copy()
        G.gmap_icos(a.g)
        return a

    def abs(self):
        a = self.copy()
        G.gmap_iabs(a.g)
        return a

    def mulunoise(self):
        a = self.copy()
        G.gmap_imulunoise(a.g)
        return a

    def mulgnoise(self):
        a = self.copy()
        G.gmap_imulgnoise(a.g)
        return a

    def setunoise(self):
        G.gmap_isetunoise(self.g)
        return self

    def setgnoise(self):
        G.gmap_isetgnoise(self.g)
        return self




class CNoise():

    def __init__(self,width,height,div,seed):
        self.c = c_void_p(G.cnoise_create(width,height,div,seed))
        self.f0 = GMap(0,0,c_void_p(G.cnoise_render(self.c,c_double(1),c_double(0),c_double(0),c_double(0))))
        self.f1 = GMap(0,0,c_void_p(G.cnoise_render(self.c,c_double(0),c_double(1),c_double(0),c_double(0))))
        self.f2 = GMap(0,0,c_void_p(G.cnoise_render(self.c,c_double(0),c_double(0),c_double(1),c_double(0))))

    def __del__(self):
        G.cnoise_destroy(self.c)

    def dump(self):
        G.cnoise_dump(self.c)
