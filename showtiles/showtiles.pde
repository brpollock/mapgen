PImage[] img;
int tw,th;
boolean redraw = true;
boolean repeat = false;

void setup() {
  size(640, 500);

  img = new PImage[20];

  for(int i =0;i<img.length;i++) {
    String fname = "grass"+nf(i,2)+".png";
    img[i] = loadImage(fname);
    println(fname+" "+img[0]);
  }
  tw = img[0].width;
  th = img[0].height;

}

void draw() {
  if(redraw) {
    redraw = false;
    tiles();
  }

}

void tiles() {
  int tile = -1;
  for(int y = 0; y < height; y += th) {
    for(int x = 0; x < width; x += tw) {
      if( tile < 0 || !repeat)
        tile = int(random(img.length));
      image(img[tile], x, y);
    }
  }
}

void keyPressed()
{
  if(key == 'r') {
    repeat = !repeat;
    println(repeat? "repeated tile" : "random tiles");
}
  redraw = true;
}
