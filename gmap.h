
#ifndef _GMAP_H_
#define _GMAP_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// gmap image

typedef struct {
    int w;
    int h;
    size_t sz;
    double p[];
} gmap_t;

void gmap_destroy(gmap_t *g);
gmap_t *gmap_create(int w,int h);
gmap_t *gmap_copy(gmap_t *g);
void gmap_clear(gmap_t *g);

void gmap_save_rgb(gmap_t *r,gmap_t *g, gmap_t *b, const char *fname);
void gmap_save(gmap_t *g,const char *fname);
void gmap_save_csv(gmap_t *g, const char *fname);

void gmap_set(gmap_t *g,int x,int y,double v);
void gmap_ithreshold(gmap_t *g, double thresh);
double gmap_min(gmap_t *g);
double gmap_max(gmap_t *g);
void gmap_iadd(gmap_t *g, double x);
void gmap_imul(gmap_t *g, double x);
void gmap_iaddm(gmap_t *g,int dx,int dy,gmap_t *s);
void gmap_imulm(gmap_t *g,int dx,int dy,gmap_t *s);
void gmap_iinvert(gmap_t *g);
void gmap_imaximize(gmap_t *g);
void gmap_ihblur(gmap_t *g);
void gmap_ivblur(gmap_t *g);
void gmap_iblur(gmap_t *g, int n);
gmap_t *gmap_hhalf(gmap_t *g);
gmap_t *gmap_vhalf(gmap_t *g);
gmap_t *gmap_quarter(gmap_t *g);
void gmap_dump(gmap_t *g);

void gmap_iclamp(gmap_t *g);
void gmap_ismoothstep(gmap_t *g);
void gmap_isin(gmap_t *g);
void gmap_icos(gmap_t *g);
void gmap_iabs(gmap_t *g);
void gmap_imulunoise(gmap_t *g);
void gmap_imulgnoise(gmap_t *g);
void gmap_isetunoise(gmap_t *g);
void gmap_isetgnoise(gmap_t *g);
#endif // _GMAP_H_
