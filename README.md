# mapgen

It's a while since I've looked at this.

There are a few things here:

- doing arithmetic with whole images
- cellular noise
- ...

This requires a C compiler, and imagemagick. (And, maybe, a Mac)

```
make
python3 test.py
```

Produces a bunch of .pgm images.

