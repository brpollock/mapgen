from pygmap import GMap as G, CNoise as C,save_rgb
import math

sz = 128

n = G(sz,sz) + 1
m = G(sz,sz) + 1

n = n.mulunoise()
n.save("noise1.pgm")
n = n * 0.5 + 0.5
n.save("noise2.pgm")


n.setunoise()
stones = (n.blur(1).maximize()*4-3).clamp().blur(1).maximize()
stonem = (stones*4).clamp().blur(1).smoothstep()
stonei = ~stonem

dirt   = n.setgnoise().blur(1)
stones.save("stones.pgm")
stonem.save("stonem.pgm")
dirt.save("dirt.pgm")
(stones*0.5 + dirt*0.5).save("ground.pgm")

# grass tiles...

ntile = 20

b = 1
border = G(sz,sz)
middle = G(sz,sz)
for y in range(sz):
    for x in range(sz):
        if x<b or y<b or x>=sz-b or y>=sz-b:
            border.set(x,y,1.0)
middle = ~border

a0 = G(sz,sz).setgnoise()
b0 = G(sz,sz).setgnoise()
c0 = G(sz,sz).setgnoise()
d0 = G(sz,sz).setgnoise()

for i in range(ntile):
    a = G(sz,sz).setgnoise()
    b = G(sz,sz).setgnoise()
    c = G(sz,sz).setgnoise()
    d = G(sz,sz).setgnoise()
    if True:
        a = a0*border + a*middle
        b = b0*border + b*middle
        c = c0*border + c*middle
        d = d0*border + b*middle

    a = a*0.4 - 0.2
    b = b*0.4 - 0.2
    a = a.hblur()*0.5 + b.vblur()*0.5
    a = a.blur(1)

    # g.set(1,1,1.0) # Where _are_ the edges?

    save_rgb(
        a+.24 - c*0.02,
        a+.32 - d*0.02,
        a+.04,
        "grass{0:0>2}.ppm".format(i))


stones = (stones*0.5)+0.25
rstones = stones * .56
gstones = stones * .48
bstones = stones * .52
save_rgb(
    (a+.17+c*.02)*stonei+stonem*rstones,
    (a+.14)*stonei+stonem*gstones,
    (a+.07)*stonei+stonem*bstones,
    "gstone.ppm".format(i))

