from pygmap import GMap as G, CNoise as C
import math

# make images at different scales
powers = [0, 1, 2]
scales = [2**p for p in powers]
n = [C(640*s, 640*s, 64, s) for s in scales]
i = [e.f2 - e.f1 for e in n]

# scale down to same size and threshold
s = [e.quarter(p) for e,p in zip(i,powers)]
t = [e.threshold(0.1) for e in s]
t[0].save("t0.pgm")
t[1].save("t1.pgm")

# invert to get a mask for the 'floor'
f = [~e for e in t]
f[0].save("f0.pgm")
f[1].save("f1.pgm")
f[2].save("f2.pgm")

# blur to add curvature to surfaces
b = [e.blur(8) for e in t]
b[0].save("b0.pgm")

# add directional light
theta = [e * math.pi - math.pi/2 for e in b]
dl = [e.sin().abs() for e in theta]
dl[0].save("dl0.pgm")
dl[1].save("dl1.pgm")
dl[2].save("dl2.pgm")

# simulate ambient occlusion by
# 1. _really_ blurring the image
# 2. using blurred for low parts, unblurred for high parts

ao = dl[1].blur(20)
high = dl[1] * b[1]
low = ao * ~b[1]
cao = high+low
high.save("high.pgm")
low.save("low.pgm")
cao.save("cao.pgm")

# alternative ao ..
ao2 = ~b[1]
ao2 = ao2.blur(20)
ao.save("ao1.pgm")
ao2.save("ao2.pgm")

low = ao2 * ~b[1]
cao = high+low
cao.save("cao2.pgm")


# combine...

flr = dl[1] * ~t[0]
hi  = dl[0] * t[0]
com = flr + hi
com.save("com.pgm")



#####################
# experiments..
a = t[0]*0.75 + t[1]*0.25
a.save("a.pgm")

b = t[0].blur(8)
p = b * math.pi - math.pi/2
s = p.sin().abs()
# print i
i[0].save("i0.pgm")
s.save("sin.pgm")
t[0].quarter(4).save("tq.pgm")
c = p.cos().abs()
c.save("cos.pgm")

one = G(400,400)
one += 1
q = b*2 - 1
q = one - q.abs()
q.save("quad.pgm")

x = s*0.5 + n[0].f0*0.5
x.save("x0.pgm")
x = s*0.5 + n[0].f1*0.5
x.save("x1.pgm")
x = s*0.5 + n[0].f2*0.5
x.save("x2.pgm")




