ALL: mg gmlib.so tblur showtiles/data/

mg: mg.c gmlib.so makefile
	gcc -o mg mg.c gmlib.so -lm

gmlib.so: gmap.c gmap.h cnoise.c cnoise.h makefile
	gcc -ggdb gmap.c cnoise.c -fPIC -shared -o gmlib.so

tblur: tblur.c gmlib.so makefile
	gcc -o tblur tblur.c gmlib.so -lm

showtiles/data/: noise.py
	python3 noise.py
	mkdir -p showtiles/data
	mv grass*.ppm showtiles/data
	mogrify -format png showtiles/data/*.ppm
	rm showtiles/data/*.ppm

