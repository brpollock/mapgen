#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "gmap.h"
#include "cnoise.h"


int main(void)
{
    int i,x,y;
    cnoise_t *n1 = cnoise_create(250,250,50,11);
    cnoise_t *n2 = cnoise_create(500,500,50,22);
    gmap_t *r1 = cnoise_render(n1,0,-1,1,0);
    gmap_t *r2 = cnoise_render(n2,0,-1,1,0);
    gmap_t *s2 = gmap_quarter(r2);
    gmap_t *f0 = cnoise_render(n1,1,0,0,0);
    gmap_t *f1 = cnoise_render(n1,0,1,0,0);
    gmap_t *f2 = cnoise_render(n1,0,0,1,0);
    gmap_t *b1,*b2;
   

    gmap_t *t1 = gmap_copy(r1);
    gmap_t *t2 = gmap_copy(s2);
    gmap_ithreshold(t1, 0.06);
    gmap_ithreshold(t2, 0.06);

    gmap_imulm(f1,0,0,t1);
    gmap_imulm(f2,0,0,t1);
    gmap_save(f0,"f0.pgm");
    gmap_save(f1,"f1.pgm");
    gmap_save(f2,"f2.pgm");

    b1 = gmap_copy(t1);
    b2 = gmap_copy(t2);
    gmap_iblur(b1,4);
    gmap_iblur(b2,4);

    gmap_imul(b2,0.25);
    gmap_imulm(b2,0,0,t1);
    gmap_imul(b1,0.75);
    gmap_iaddm(b1,0,0,b2);
    gmap_save(b1,"t1.pgm");
    gmap_save(b2,"t2.pgm");

    cnoise_destroy(n1);
    cnoise_destroy(n2);
    gmap_destroy(r1);
    gmap_destroy(r2);
    gmap_destroy(s2);
    return 0;
}

